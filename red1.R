
pacman::p_load(intergraph,
               tidygraph,
               igraph,
               network,
               sna,
               ggplot2,
               xlsx,
               ggraph,
               backbone,
               dplyr,
               readr)


## Cargar base
load("ELSOC_W03_v1.10_R.RData")

asociaciones = elsoc_2018 %>%
  dplyr::mutate (JJVV         = ifelse(c12_01<2, 0, 1)) %>%
  dplyr::mutate (Religiosa    = ifelse(c12_02<2, 0, 1)) %>%
  dplyr::mutate (Partido      = ifelse(c12_03<2, 0, 1)) %>%
  dplyr::mutate (Deportiva    = ifelse(c12_07<2, 0, 1)) %>%
  dplyr::mutate (AAEE         = ifelse(c12_08<2, 0, 1)) %>%
  dplyr::mutate (Caridad      = ifelse(c12_06<2, 0, 1)) %>%
  dplyr::mutate (Sindicato    = ifelse(c12_04<2, 0, 1)) %>%
  dplyr::mutate (Profesional  = ifelse(c12_05<2, 0, 1)) %>%
  dplyr::mutate (Otras        = ifelse(c12_09<2, 0, 1))%>%
  dplyr::select (JJVV, Religiosa, AAEE, Deportiva, Partido, 
                 Profesional, Sindicato, Caridad, Otras)


asociaciones[asociaciones=="-999"] <- NA
asociaciones[asociaciones=="-888"] <- NA
asociaciones <- na.omit(asociaciones)
asociaciones <- as.matrix(asociaciones)

#Transpose
asociaciones1 <-t(asociaciones)

#Proyección modo 1 (overlap) 
A <- asociaciones1%*%t(asociaciones1)
write.xlsx (A, file = "A.xlsx")

hyperg <- hyperg(asociaciones1)
hyperg_bb <- backbone.extract(hyperg, signed = TRUE)
hyperg_bb

## Stochastic Degree Sequence Model
sdsm <- sdsm(asociaciones1, model = "cloglog")
sdsm_bb <- backbone.extract(sdsm, signed = TRUE, alpha = 0.01) 
sdsm_bb
summary(sdsm_bb)


##Graficar estructura de asociaciones 
sdsm_bb <- as.matrix(sdsm_bb)
write.xlsx(sdsm_bb, "sdsm_bb1.xlsx", sheetName="Sheet1", col.names=TRUE, 
           row.names=TRUE, append=FALSE)
stata1 <- read.xlsx("sdsm_bb1.xlsx", sheetName="Sheet1", row.names=T)



# Graficar resultados obtenidos con STATA ---------------------------------

## Visualización de red con tidiverse -------------------------------------

nodos <- read_csv("nodos.csv")
lazos <- read_csv("lazos.csv")

nodos
lazos

g <- graph_from_data_frame(lazos, directed = F, vertices = nodos)
g
V(g) 
vcount(g)
E(g)
ecount(g)

# agregar atributos a la red
g$name <- "Red asociativa"
g$name

V(g)$id <- 1:9
V(g)$id
V(g)$tipo
E(g)$weight
g

# Visualización de la red
ggraph(g, layout = "in_circle") +  
       theme_graph(background = "white",
                   base_family = "Arial Narrow",
                   base_size = 11) + 
  geom_edge_link(aes(color = vinculo)) + 
  geom_node_point(alpha = 8, size = 2) + 
  geom_node_text(aes(label = name), repel = T) +
  facet_edges(~vinculo)
 
?ggraph

  
ggraph:::igraphlayouts


## Simulación de datos para probar ajuste a la distribución de link usado 

## LOGIT
# Convert from the logit scale to a probability
p <- plogis(0)

# Simulate a logit 
rbinom(n = 10, size = 1, p)

##PROBIT 
# Convert from the probit scale to a probability
p <- pnorm(0)

# Simulate a probit
rbinom(n = 10, size = 1, p)


## Graficar regresión logit

# Create a jittered plot of MilesOneWay vs Bus2 using the bus dataset
ggJitter <- ggplot(data = bus, aes(x = MilesOneWay, y = Bus2)) + 
  geom_jitter(width = 0, height = 0.05) +
  ylab("Probability of riding the bus") +
  xlab("One-way commute trip (in miles)")

# Add a geom_smooth() that uses a GLM method to your plot
ggJitter + geom_smooth(method =  "glm" , method.args = list(family = "binomial"))



# Add geom_smooth() lines for the probit and logit link functions
ggJitter + 
  geom_smooth(method = 'glm', 
              method.args = list(family = binomial(link = 'probit')), 
              color = 'red', se = F) +
  geom_smooth(method = 'glm', 
              method.args = list(family = binomial(link = "logit")), 
              color = 'blue', se = F)

# Plot linear effect of travel distance on probability of taking the bus
ggJitter <- ggplot(data = bus, aes(x = MilesOneWay, y = Bus2)) + 
  geom_jitter(width = 0, height = 0.05) + 
  geom_smooth(method = 'glm', 
              method.args = list(family = 'binomial'))

# Add a non-linear equation to a geom_smooth()
ggJitter + geom_smooth(method = 'glm', 
                       method.args = list(family = 'binomial'), 
                       formula = y~I(x^2), 
                       color = 'red')