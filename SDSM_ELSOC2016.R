pacman::p_load(intergraph,
               tidygraph,
               igraph,
               network,
               sna,
               ggplot2,
               xlsx,
               ggraph,
               backbone,
               dplyr,
               readr,
               viridis,
               scales,
               circlize)


## Cargar base
load("ELSOC_W01_v3.10_R.RData")

asociaciones = elsoc_2016 %>%
  dplyr::mutate (JJVV        = ifelse   (c12_01     < 2 , 0,    1)) %>% 
  dplyr::mutate (Religiosa   = ifelse   (c12_02     < 2 , 0,    1)) %>%
  dplyr::mutate (Partido     = ifelse   (c12_03     < 2 , 0,    1)) %>%
  dplyr::mutate (Sindicato   = ifelse   (c12_04     < 2 , 0,    1)) %>%
  dplyr::mutate (Profesional = ifelse   (c12_05     < 2 , 0,    1)) %>%
  dplyr::mutate (Caridad     = ifelse   (c12_06     < 2 , 0,    1)) %>%
  dplyr::mutate (Deportiva   = ifelse   (c12_07     < 2 , 0,    1)) %>%
  dplyr::mutate (AAEE        = ifelse   (c12_08     < 2 , 0,    1)) %>%
  dplyr::mutate (Otras       = ifelse   (c12_09     < 2 , 0,    1)) %>%
  dplyr::select (JJVV, Religiosa, Partido, Sindicato, Profesional, Caridad, 
                 Deportiva, AAEE, Otras)


asociaciones[asociaciones=="-999"] <- NA
asociaciones[asociaciones=="-888"] <- NA
asociaciones <- na.omit(asociaciones)
asociaciones <- as.matrix(asociaciones)

#Transpose
asociaciones1 <-t(asociaciones)

#Proyección modo 1 (overlap) 
A <- asociaciones1%*%t(asociaciones1)

A<-as.data.frame(A)

## Graficar overlap ------------------------------------
# color palette
micolor<-c(JJVV="#000004FF", Religiosa="#210C4AFF", AAEE="#56106EFF", 
           Deportiva="#89226AFF", Partido="#BB3754FF", 
           Profesional="#E35932FF", Sindicato="#F98C0AFF", 
           Caridad="#F9C932FF", Otras="#FCFFA4FF")

A<-as.matrix(A)

chordDiagram(A, 
             grid.col = micolor, 
             directional = 2,
             diffHeight  = -0.02,
             annotationTrack = c("name", "grid"),
             annotationTrackHeight = c(0.05, 0.05),
             link.arr.type = "big.arrow", 
             link.sort = TRUE, 
             link.largest.ontop = TRUE,
             scale = FALSE,
             transparency = 0.4)

## Stochastic Degree Sequence Model --------------------------------------
sdsm <- sdsm(asociaciones1, model = "cloglog") 
#Con model = polytope se calcula el ajuste a los datos de varias distribuciones
sdsm_bb <- backbone.extract(sdsm, signed = TRUE, alpha = 0.05) 
sdsm_bb
summary(sdsm_bb)


## Visualización de red con tidiverse -------------------------------------
## La información de la matriz
nodos <- read_csv("nodos.csv")
lazos <- read_csv("lazos.csv")

nodos
lazos

g <- graph_from_data_frame(lazos, directed = F, vertices = nodos)
g
V(g) 
vcount(g)
E(g)
ecount(g)

# agregar atributos a la red
g$name <- "Red asociativa"
g$name

V(g)$id <- 1:9
V(g)$id
V(g)$tipo
E(g)$weight
g

# Visualización de la red
ggraph(g, layout = "with_kk") +  
  theme_graph(background = "white",
              base_family = "Arial Narrow",
              base_size = 11) + 
  geom_edge_link(aes(color = vinculo)) + 
  geom_node_point(aes(shape = tipo), alpha = 8, size = 4) + 
  geom_node_text(aes(label = name), repel = T) +
  geom_edge_density(aes(fill = vinculo)) + 
  geom_edge_link(alpha = 0.01) + 
  theme(legend.text=element_text(size=12))

ggraph:::igraphlayouts

