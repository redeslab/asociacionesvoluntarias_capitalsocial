  
  ## Análisis factorial confirmatorio FCA --------------------------------
  
  library(semTools)
  library(influence.SEM)
  library(dplyr)
  library(lavaan)
  library(psych)   
  library(semPlot)
  library(car)
  library(stringr)

  ## Análisis con ELSOCW1(2016)
  ## Cargar base, seleccionar, y recodificar variables ---------------
  load("ELSOC_W01_v3.10_R.RData")
  
  ELSOC<- elsoc_2016 %>%    
  dplyr::mutate (c02         = case_when(c02       == 1      ~ 1,  
                                         c02       == 2      ~ 0, 
                                         c02       == 3      ~ 1)) %>% 
  dplyr::mutate (c03         = case_when(c03       == 1      ~ 1,
                                         c03       == 2      ~ 0,
                                         c03       == 3      ~ 1)) %>%
  dplyr::mutate (c04         = case_when(c04       == 1      ~ 0,
                                         c04       == 2      ~ 1,
                                         c04       == 3      ~ 1)) %>% 
  dplyr::mutate (extranjero  = case_when(m45     %in% 1       ~ 0,
                                         m45     %in% 2 :8    ~ 1)) %>% 
  dplyr::mutate (mujer       = case_when(m0_sexo   == 1       ~ 0,
                                         m0_sexo   == 2       ~ 1)) %>% 
  dplyr::mutate (edad        = case_when(m0_edad %in% 18:24   ~ "18-24",
                                         m0_edad %in% 25:34   ~ "25-34",
                                         m0_edad %in% 35:44   ~ "35-44",
                                         m0_edad %in% 45:54   ~ "45-54",
                                         m0_edad %in% 55:64   ~ "55-64", 
                                         m0_edad %in% 65:88   ~ "65-")) %>%
  dplyr::mutate (nivel_educ  = case_when(m01     %in% 1 :3    ~ "básica",
                                         m01     %in% 4 :5    ~ "media",
                                         m01     %in% 6 :7    ~ "técnica",
                                         m01     %in% 8 :10   ~ "universitaria")) %>%
  dplyr::mutate (JJVV        =    ifelse(c12_01     < 2 ,0   , 1)) %>% 
  dplyr::mutate (religiosa   =    ifelse(c12_02     < 2 ,0   , 1)) %>%
  dplyr::mutate (partido.mov =    ifelse(c12_03     < 2 ,0   , 1)) %>%
  dplyr::mutate (sindicato   =    ifelse(c12_04     < 2 ,0   , 1)) %>%
  dplyr::mutate (profesional =    ifelse(c12_05     < 2 ,0   , 1)) %>%
  dplyr::mutate (caridad     =    ifelse(c12_06     < 2 ,0   , 1)) %>%
  dplyr::mutate (deportiva   =    ifelse(c12_07     < 2 ,0   , 1)) %>%
  dplyr::mutate (AAEE        =    ifelse(c12_08     < 2 ,0   , 1)) %>%
  dplyr::mutate (otra        =    ifelse(c12_09     < 2 ,0   , 1)) %>%
  dplyr::select (JJVV, religiosa, partido.mov, sindicato, profesional, 
                 caridad, AAEE, deportiva, otra, r01_01, r01_02, r01_03, 
                 r01_04, r01_05, r01_06, r01_07, r01_08, r01_09, r01_10, 
                 r01_11, r01_12, r01_13, c02, c03, c04, mujer, 
                 edad, nivel_educ, salud = s03, ponderador01)

 ## Casos perdidos 
  
  ELSOC[ELSOC=="-999"] <- NA
  ELSOC[ELSOC=="-888"] <- NA
  ELSOC=na.omit(ELSOC)
  
 ## Ajustar modelo de ecuaciones estructurales SEM
 
  modelo.div <- "
  
  # Factoriales
  
  DCM          =~ r01_03 + r01_12 + r01_06 + r01_08 + r01_01 + r01_13 + r01_02  
  DCB          =~ r01_07 + r01_05 + r01_09 + r01_11 + r01_04 + r01_10 
  CONF         =~ c02 + w*c03 + w*c04
  AI           =~ caridad + AAEE + partido.mov + profesional + sindicato   
  AE           =~ deportiva + JJVV + religiosa 
  
  # Regresiones 
  
  DCM          ~ a*AE + h*AI + edad + salud + mujer + nivel_educ 
  DCB          ~ d*AE + f*AI + edad + salud + mujer + nivel_educ 
  CONF         ~ b*DCM + e*DCB + c*AE + g*AI + edad + salud + mujer + nivel_educ 
         
  # Efectos directos e indirectos
  
  indirecto1   := a*b
  indirecto2   := d*e
  directo1     := c
  total1       := c + (a*b) + (d*e)
   
  indirecto3   := f*e
  indirecto4   := h*b
  directo2     := g
  total2       := g + (h*b) + (e*f)
  
  # Correlaciones entre mediadores 
  
  DCM          ~~ DCB
  
  # Correciones sugeridas
  
  "
  
  #Evaluar ajuste del modelo SEM --------------------------
  
  div.fit <- sem(modelo.div, data = ELSOC, std.lv=F,  
                 ordered = c("partido.mov", "sindicato", "profesional", 
                             "AAEE", "JJVV", "religiosa", "caridad", 
                             "deportiva", "c02", "c03", "c04"), 
                 sampling.weights = "ponderador01", 
                 estimator = "ULSMV")
                 
  #Evaluar
  
  summary(div.fit, 
          fit.measure=TRUE, 
          standardized=TRUE, 
          rsquare = TRUE)
  

  parameterestimates(div.fit, standardized=TRUE) ##CIs para parámetros
  fitted(div.fit) ##Tabla de covarianzas 
  residuals(div.fit) ##Residuos
  fitmeasures(div.fit) ##Indices de ajuste
  modificationindices(div.fit, sort. = TRUE) ##Indices de modifación 
  

  # Graficar SEM --------------------------------
  
  # sacar p values no significtaivos---------------------
  
  lavaan::standardizedSolution(div.fit) %>% 
    dplyr::filter(!is.na(pvalue)) %>% 
    arrange(desc(pvalue)) %>% 
    mutate_if("is.numeric","round",3) %>% 
    select(-ci.lower,-ci.upper,-z)
  
  pvalue_cutoff <- 0.05
  
  obj <- semPlot:::semPlotModel(div.fit)
  
  # Copia del original. Compararlo más tarde y asegurarnos de eliminar solo lo que pretendíamos 
  original_Pars <- obj@Pars
  
  check_Pars <- obj@Pars %>% 
    dplyr::filter(!(edge %in% c("int","<->") | lhs == rhs)) # Lista de parámetros
  
  keep_Pars <- obj@Pars %>% 
    dplyr::filter(edge %in% c("int","<->") | lhs == rhs) # Parámetros que se mantienen
  
  test_against <- lavaan::standardizedSolution(div.fit) %>%
    dplyr::filter(pvalue < pvalue_cutoff, rhs != lhs)
  
  test_against_rev <- test_against %>% 
    rename(rhs2 = lhs, lhs = rhs) %>% # revertir y probar
    rename(rhs = rhs2)
  
  checked_Pars <-
    check_Pars %>% 
    semi_join(test_against, by = c("lhs", "rhs")) %>% bind_rows(
      check_Pars %>% 
        semi_join(test_against_rev, by = c("lhs", "rhs"))
    )
  
  obj@Pars <- keep_Pars %>% bind_rows(checked_Pars)
  
  # verificar
  anti_join(original_Pars,obj@Pars)
  
  # plot
  semPlot::semPaths(obj, "std",fade = F, residuals = F,intercept = FALSE, 
                    exoCov = FALSE, layout = "tree2", thresholds = FALSE, 
                    sizeMan=4.6, sizeLat = 9, centerLevels = T,  
                    nCharNodes=6,  optimizeLatRes = F, reorder = F, 
                    structural = TRUE, edge.label.cex = 1)
  
  #
  
#### Plot 
#```{r}
#
#lavaan::standardizedSolution(fit.sem1) %>% dplyr::filter(!is.na(pvalue)) %>% dplyr::arrange(desc(pvalue)) #%>% dplyr::mutate_if("is.numeric","round",3) %>% dplyr::select(-ci.lower,-ci.upper,-z) # dejar solo los #paths significativos
#
#
#  pvalue_cutoff <- 0.05
#  
#  obj <- semPlot:::semPlotModel(fit.sem1)
#  
#  # Copia del original. Compararlo más tarde y asegurarnos de eliminar solo lo que pretendíamos 
#  original_Pars <- obj@Pars
#  
#  check_Pars <- obj@Pars %>% 
#    dplyr::filter(!(edge %in% c("int","<->") | lhs == rhs)) # Lista de parámetros
#  
#  keep_Pars <- obj@Pars %>% 
#    dplyr::filter(edge %in% c("int","<->") | lhs == rhs) # Parámetros que se mantienen
#  
#  test_against <- lavaan::standardizedSolution(fit.sem1) %>%
#    dplyr::filter(pvalue < pvalue_cutoff, rhs != lhs)
#  
#  test_against_rev <- test_against %>% 
#    rename(rhs2 = lhs, lhs = rhs) %>% # revertir y probar
#    rename(rhs = rhs2)
#  
#  checked_Pars <-
#    check_Pars %>% 
#    semi_join(test_against, by = c("lhs", "rhs")) %>% bind_rows(
#      check_Pars %>% 
#        semi_join(test_against_rev, by = c("lhs", "rhs"))
#    )
#  
#  obj@Pars <- keep_Pars %>% bind_rows(checked_Pars)
#  
#  # verificar
#  anti_join(original_Pars,obj@Pars)
#
#  
#  # plot
#  semPlot::semPaths(obj, "std",fade = F, residuals = F,intercept = FALSE, 
#                    exoCov = T,layout = "tree2", thresholds = FALSE, 
#                    sizeMan=6, sizeLat = 10, centerLevels = T,  
#                    nCharNodes=7,  optimizeLatRes = T, reorder = F, 
#                    structural =F, edge.label.cex = .8, curvePivot = F,
#                    label.scale=TRUE, sizeMan2=2.5, asize=1, edge.color="black", 
#         edge.label.color="black", edge.label.position=.8, edge.label.margin=-.1,
#         width=17, height=40)
#
#```



  # evaluar invarianza entre grupos ---------------------------
  
  
  
