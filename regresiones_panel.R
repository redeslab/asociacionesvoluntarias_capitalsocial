# Modelos multinivel longitudinal (within-between) para asociaciones voluntarias -------------------------------
pacman::p_load(tidyverse,
               jtools,
               ggplot2,
               broom,
               survey,
               dplyr,
               survey,
               kableExtra,
               scales,
               panelr)

## Cargar base
load("ELSOC_W01_v3.10_R.RData")
load("ELSOC_W03_v1.10_R.RData")

# Selección de variables
elsoc1<-dplyr::select(elsoc_2016, idencuesta, ponderador01, m0_sexo, m0_edad, 
                      m01, m29, c02, t01, m45, c12_01, c12_02, c12_03, c12_04, 
                      c12_05, c12_06, c12_07, c12_08, c12_09, r01_01, r01_02, 
                      r01_03, r01_04, r01_05, r01_06, r01_07, r01_08, r01_09, 
                      r01_10, r01_11, r01_12, r01_13)

elsoc2<-dplyr::select(elsoc_2018, idencuesta, ponderador01, m0_sexo, m0_edad, 
                      m01, m29, c02, t01, m45, c12_01, c12_02, c12_03, c12_04, 
                      c12_05, c12_06, c12_07, c12_08, c12_09, r01_01, r01_02, 
                      r01_03, r01_04, r01_05, r01_06, r01_07, r01_08, r01_09, 
                      r01_10, r01_11, r01_12, r01_13)

# Generar identificador de ola y setear bases
elsoc1$ola<-1
elsoc2$ola<-2
elsoc<-rbind(elsoc1,elsoc2)

# Generar subbase auxiliar para contar los id agrupados 
#Si están en las dos olas apareceran dos veces, sino una sola vez
xx<-elsoc%>% 
  group_by(idencuesta)%>%
  summarise(n_id=n())
#Pegamos la base auxiliar xx a la base red y filtramos mantieniendo solo 
#aquellos casos que estén dos veces 
elsoc<-left_join(elsoc, xx, by="idencuesta") 
elsoc<-elsoc[elsoc$n_id == 2,]


#Recodificación de variables ------------------------------------

##Recodificar asociaciones voluntarias y otras

elsoc = elsoc %>%
  dplyr::mutate (conf_gral   = case_when(c02       == 1       ~ 1,  
                                         c02       == 2       ~ 0, 
                                         c02       == 3       ~ 1)) %>%
  dplyr::mutate (extranjero  = case_when(m45     %in% 1       ~ 0,
                                         m45     %in% 2 :8    ~ 1)) %>% 
  dplyr::mutate (mujer       = case_when(m0_sexo   == 1       ~ 0,
                                         m0_sexo   == 2       ~ 1)) %>% 
  dplyr::mutate (edad        = case_when(m0_edad %in% 18:24   ~ "18-24",
                                         m0_edad %in% 25:34   ~ "25-34",
                                         m0_edad %in% 35:44   ~ "35-44",
                                         m0_edad %in% 45:54   ~ "45-54",
                                         m0_edad %in% 55:64   ~ "55-64", 
                                         m0_edad %in% 65:88   ~ "65-")) %>%
  dplyr::mutate (nivel_educ  = case_when(m01     %in% 1 :3    ~ "básica",
                                         m01     %in% 4 :5    ~ "media",
                                         m01     %in% 6 :7    ~ "técnica",
                                         m01     %in% 8 :10   ~ "univers")) %>%
  dplyr::mutate (r01_01      = ifelse   (r01_01     > 1 , 1,    0)) %>%
  dplyr::mutate (r01_02      = ifelse   (r01_02     > 1 , 1,    0)) %>%
  dplyr::mutate (r01_03      = ifelse   (r01_03     > 1 , 1,    0)) %>%
  dplyr::mutate (r01_04      = ifelse   (r01_04     > 1 , 1,    0)) %>%
  dplyr::mutate (r01_05      = ifelse   (r01_05     > 1 , 1,    0)) %>%
  dplyr::mutate (r01_06      = ifelse   (r01_06     > 1 , 1,    0)) %>%
  dplyr::mutate (r01_07      = ifelse   (r01_07     > 1 , 1,    0)) %>%
  dplyr::mutate (r01_08      = ifelse   (r01_08     > 1 , 1,    0)) %>%
  dplyr::mutate (r01_09      = ifelse   (r01_09     > 1 , 1,    0)) %>%
  dplyr::mutate (r01_10      = ifelse   (r01_10     > 1 , 1,    0)) %>%
  dplyr::mutate (r01_11      = ifelse   (r01_11     > 1 , 1,    0)) %>%
  dplyr::mutate (r01_12      = ifelse   (r01_12     > 1 , 1,    0)) %>%
  dplyr::mutate (r01_13      = ifelse   (r01_13     > 1 , 1,    0)) %>%
  dplyr::mutate (JJVV        = ifelse   (c12_01     < 2 , 0,    1)) %>% 
  dplyr::mutate (religiosa   = ifelse   (c12_02     < 2 , 0,    1)) %>%
  dplyr::mutate (partido_mov = ifelse   (c12_03     < 2 , 0,    1)) %>%
  dplyr::mutate (sindicato   = ifelse   (c12_04     < 2 , 0,    1)) %>%
  dplyr::mutate (profesional = ifelse   (c12_05     < 2 , 0,    1)) %>%
  dplyr::mutate (caridad     = ifelse   (c12_06     < 2 , 0,    1)) %>%
  dplyr::mutate (deportiva   = ifelse   (c12_07     < 2 , 0,    1)) %>%
  dplyr::mutate (AAEE        = ifelse   (c12_08     < 2 , 0,    1)) %>%
  dplyr::mutate (otra        = ifelse   (c12_09     < 2 , 0,    1)) %>%
  dplyr::select (idencuesta, ola, ponderador01, mujer, nivel_educ, edad, 
                 conf_gral, conf_vec=t01, extranjero, JJVV, religiosa, 
                 partido_mov, sindicato, profesional, caridad, deportiva, 
                 AAEE, otra, r01_01, r01_02, r01_03, r01_04, r01_05, r01_06, 
                 r01_07, r01_08, r01_09, r01_10, r01_11, r01_12, r01_13)

elsoc[elsoc=="-999"] <- NA
elsoc[elsoc=="-888"] <- NA
elsoc <- na.omit(elsoc)


#variable diversidad
elsoc <- elsoc %>%
  mutate(diversidad = r01_01 + r01_02 + r01_03 + r01_04 + r01_05 + r01_06 + 
           r01_07 + r01_08 + r01_09 + r01_10 + r01_11 + r01_12 + r01_13)

##Crear variables de diversidad de clase media
elsoc <- elsoc %>%
  mutate(diversidadCM = r01_01 + r01_06 + r01_08 + r01_09 + r01_12 + r01_13)

##Crear variable diversidad de lazos de clase trabajadora
elsoc <- elsoc %>%
  mutate(diversidadCT = r01_02 + r01_03 + r01_04 + r01_05 + r01_07 + 
           r01_10 + r01_11)

save(elsoc, file="elsoclong.RData")

##transformas a panel data
elsoc_long <- panel_data(elsoc, id = idencuesta, wave = ola) %>%
  complete_data(min.waves = 2)

# Modelo 1 -------------------------------------------

modelo1 <- panelr::wbm(conf_gral ~ JJVV + religiosa + partido_mov + sindicato + 
                         profesional + caridad + deportiva + AAEE + otra | 
                         mujer + extranjero + edad + nivel_educ,
               weights = ponderador01, 
               model = "w-b", 
               data = elsoc_long, 
               quasibinomial(link = "logit"))

summary(modelo1)

# Modelo 2 -------------------------------------------

modelo2 <- panelr::wbm(conf_vec ~ JJVV + religiosa + partido_mov + sindicato + 
                         profesional + caridad + deportiva + AAEE + otra | 
                         mujer + extranjero + edad + nivel_educ,
                       weights = ponderador01, 
                       model = "w-b", 
                       data = elsoc_long, 
                       quasipoisson(link = "log"))

summary(modelo2)

# Modelo 3 -------------------------------------------

modelo3 <- panelr::wbm(diversidad ~ JJVV + religiosa + partido_mov + sindicato + 
                         profesional + caridad + deportiva + AAEE + otra | 
                         mujer + extranjero + edad + nivel_educ,
                       weights = ponderador01, 
                       model = "contextual", 
                       data = elsoc_long, 
                       quasipoisson(link = "log"))

summary(modelo3)

# Modelo 4 ------------------------------------------- 

modelo4 <- panelr::wbm(diversidadCM ~ JJVV + religiosa + partido_mov + sindicato + 
                         profesional + caridad + deportiva + AAEE + otra | 
                         mujer + extranjero + edad + nivel_educ,
                       weights = ponderador01, 
                       model = "w-b", 
                       data = elsoc_long, 
                       quasipoisson(link = "log"))

summary(modelo4)

# Modelo 5 -------------------------------------------

modelo5 <- panelr::wbm(diversidadCT ~ JJVV + religiosa + partido_mov + sindicato + 
                         profesional + caridad + deportiva + AAEE + otra | 
                         mujer + extranjero + edad + nivel_educ,
                       weights = ponderador01, 
                       model = "w-b", 
                       data = elsoc_long, 
                       quasipoisson(link = "log"))

summary(modelo5)
