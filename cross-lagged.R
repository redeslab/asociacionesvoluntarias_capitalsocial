
## Cargar librerias

pacman::p_load(ggplot2,ggthemes,tidyverse,sjlabelled,sjPlot,car,vcd,texreg,ordinal,
               nnet,MASS,mlogit,matrixStats,expss,sjlabelled,sjmisc,tidyverse,
               survey,dplyr,lavaan,MASS,texreg)
          

load("/home/rober/Documents/COES ELSOC/Olas/ELSOC_W01_v3.10_R.RData")
load("/home/rober/Documents/COES ELSOC/Olas/ELSOC_W03_v1.10_R.RData")


## Cargar BBDD 2016
w1<- elsoc_2016 %>%
  dplyr::mutate (w1c02 = case_when(c02 == 1 ~ 1,  
                                   c02 == 2 ~ 0, 
                                   c02 == 3 ~ 1)) %>% 
  dplyr::mutate (w1c03 = case_when(c03 == 1 ~ 1,
                                   c03 == 2 ~ 0,
                                   c03 == 3 ~ 1)) %>%
  dplyr::mutate (w1c04 = case_when(c04 == 1 ~ 0,
                                   c04 == 2 ~ 1,
                                   c04 == 3 ~ 1)) %>%
  dplyr::mutate (w1mujer       = case_when(m0_sexo   == 1       ~ 0,
                                          m0_sexo   == 2       ~ 1)) %>% 
  dplyr::mutate (w1edad        = case_when(m0_edad %in% 18:24   ~ "18-24",
                                           m0_edad %in% 25:34   ~ "25-34",
                                           m0_edad %in% 35:44   ~ "35-44",
                                           m0_edad %in% 45:54   ~ "45-54",
                                           m0_edad %in% 55:64   ~ "55-64", 
                                           m0_edad %in% 65:88   ~ "65-")) %>%
  dplyr::mutate (w1nivel_educ  = case_when(m01     %in% 1 :3    ~ "básica",
                                           m01     %in% 4 :5    ~ "media",
                                           m01     %in% 6 :7    ~ "técnica",
                                           m01     %in% 8 :10   ~ "univers")) %>%
  dplyr::mutate (w1r01_01      = ifelse   (r01_01     > 1 , 1,    0)) %>%
  dplyr::mutate (w1r01_02      = ifelse   (r01_02     > 1 , 1,    0)) %>%
  dplyr::mutate (w1r01_03      = ifelse   (r01_03     > 1 , 1,    0)) %>%
  dplyr::mutate (w1r01_04      = ifelse   (r01_04     > 1 , 1,    0)) %>%
  dplyr::mutate (w1r01_05      = ifelse   (r01_05     > 1 , 1,    0)) %>%
  dplyr::mutate (w1r01_06      = ifelse   (r01_06     > 1 , 1,    0)) %>%
  dplyr::mutate (w1r01_07      = ifelse   (r01_07     > 1 , 1,    0)) %>%
  dplyr::mutate (w1r01_08      = ifelse   (r01_08     > 1 , 1,    0)) %>%
  dplyr::mutate (w1r01_09      = ifelse   (r01_09     > 1 , 1,    0)) %>%
  dplyr::mutate (w1r01_10      = ifelse   (r01_10     > 1 , 1,    0)) %>%
  dplyr::mutate (w1r01_11      = ifelse   (r01_11     > 1 , 1,    0)) %>%
  dplyr::mutate (w1r01_12      = ifelse   (r01_12     > 1 , 1,    0)) %>%
  dplyr::mutate (w1r01_13      = ifelse   (r01_13     > 1 , 1,    0)) %>%
  dplyr::mutate (w1JJVV        = ifelse   (c12_01     < 2 , 0,    1)) %>% 
  dplyr::mutate (w1religiosa   = ifelse   (c12_02     < 2 , 0,    1)) %>%
  dplyr::mutate (w1partido.mov = ifelse   (c12_03     < 2 , 0,    1)) %>%
  dplyr::mutate (w1sindicato   = ifelse   (c12_04     < 2 , 0,    1)) %>%
  dplyr::mutate (w1profesional = ifelse   (c12_05     < 2 , 0,    1)) %>%
  dplyr::mutate (w1caridad     = ifelse   (c12_06     < 2 , 0,    1)) %>%
  dplyr::mutate (w1deportiva   = ifelse   (c12_07     < 2 , 0,    1)) %>%
  dplyr::mutate (w1AAEE        = ifelse   (c12_08     < 2 , 0,    1)) %>%
  dplyr::mutate (w1otra        = ifelse   (c12_09     < 2 , 0,    1)) %>%
  dplyr::select (idencuesta, 
                 w1ponderador01=ponderador01, 
                 w1nivel_educ, 
                 w1edad, 
                 w1mujer, 
                 w1JJVV,
                 w1religiosa, 
                 w1partido.mov, 
                 w1sindicato, 
                 w1profesional, 
                 w1caridad,
                 w1AAEE,
                 w1deportiva,
                 w1otra,
                 w1c02,
                 w1c03,
                 w1c04)
                 
## Casos perdidos
w1[w1=="-999"] <- NA
w1[w1=="-888"] <- NA
w1=na.omit(w1)

## Cargar BBDD 2018                
w2<- elsoc_2018 %>%
  dplyr::mutate (w2c02 = case_when(c02 == 1 ~ 1,  
                                   c02 == 2 ~ 0, 
                                   c02 == 3 ~ 1)) %>% 
  dplyr::mutate (w2c03 = case_when(c03 == 1 ~ 1,
                                   c03 == 2 ~ 0,
                                   c03 == 3 ~ 1)) %>%
  dplyr::mutate (w2c04 = case_when(c04 == 1 ~ 0,
                                   c04 == 2 ~ 1,
                                   c04 == 3 ~ 1)) %>%
  dplyr::mutate (w2mujer       = case_when(m0_sexo   == 1       ~ 0,
                                           m0_sexo   == 2       ~ 1)) %>% 
  dplyr::mutate (w2edad        = case_when(m0_edad %in% 18:24   ~ "18-24",
                                           m0_edad %in% 25:34   ~ "25-34",
                                           m0_edad %in% 35:44   ~ "35-44",
                                           m0_edad %in% 45:54   ~ "45-54",
                                           m0_edad %in% 55:64   ~ "55-64", 
                                           m0_edad %in% 65:88   ~ "65-")) %>%
  dplyr::mutate (w2nivel_educ  = case_when(m01     %in% 1 :3    ~ "básica",
                                           m01     %in% 4 :5    ~ "media",
                                           m01     %in% 6 :7    ~ "técnica",
                                           m01     %in% 8 :10   ~ "univers")) %>%
  dplyr::mutate (w2JJVV        = ifelse   (c12_01     < 2 , 0,    1)) %>% 
  dplyr::mutate (w2religiosa   = ifelse   (c12_02     < 2 , 0,    1)) %>%
  dplyr::mutate (w2partido.mov = ifelse   (c12_03     < 2 , 0,    1)) %>%
  dplyr::mutate (w2sindicato   = ifelse   (c12_04     < 2 , 0,    1)) %>%
  dplyr::mutate (w2profesional = ifelse   (c12_05     < 2 , 0,    1)) %>%
  dplyr::mutate (w2caridad     = ifelse   (c12_06     < 2 , 0,    1)) %>%
  dplyr::mutate (w2deportiva   = ifelse   (c12_07     < 2 , 0,    1)) %>%
  dplyr::mutate (w2AAEE        = ifelse   (c12_08     < 2 , 0,    1)) %>%
  dplyr::mutate (w2otra        = ifelse   (c12_09     < 2 , 0,    1)) %>%
  dplyr::select (idencuesta, 
                 w2ponderador01=ponderador01, 
                 w2nivel_educ, 
                 w2edad, 
                 w2mujer, 
                 w2JJVV,
                 w2religiosa, 
                 w2partido.mov, 
                 w2sindicato, 
                 w2profesional, 
                 w2caridad,
                 w2AAEE,
                 w2deportiva,
                 w2otra,
                 w2c02,
                 w2c03,
                 w2c04)                
                 
## Eliminar casos perdidos
w2[w2=="-999"] <- NA
w2[w2=="-888"] <- NA
w2=na.omit(w2)

## Crear BBDD wide
w1w2_wide<-left_join(w1,w2,by="idencuesta") 
save(w1w2_wide, file = "w1w2_wide.RData")

## 



# Definir modelo ---------------------

modelo.conf <- "
# Definimos variables
w1ai   =~ NA*w1AAEE + (lambda1)*w1AAEE + (lambda2)*w1partido.mov + (lambda3)*w1sindicato + (lambda4)*w1profesional + (lambda5)*w1caridad
w2ai   =~ NA*w2AAEE + (lambda1)*w2AAEE + (lambda2)*w2partido.mov + (lambda3)*w2sindicato + (lambda4)*w2profesional + (lambda5)*w2caridad

w1ae   =~ NA*w1JJVV + (lambda6)*w1JJVV + (lambda7)*w1religiosa + (lambda8)*w1deportiva 
w2ae   =~ NA*w2JJVV + (lambda6)*w2JJVV + (lambda7)*w2religiosa + (lambda8)*w2deportiva 

w1conf =~ NA*w1c03 + (lambda9)*w1c03 + (lambda10)*w1c02 + (lambda11)*w1c04
w2conf =~ NA*w2c03 + (lambda9)*w2c03 + (lambda10)*w2c02 + (lambda11)*w2c04

# Crosslagged y efectos autoregresivos

w2ai   ~ w1ai + w1conf + w1ae 
w2ae   ~ w1ae + w1conf + w1ai 
w2conf ~ w1conf + w1ai + w1ae 
w2ae   ~ w2ai 

# correlated measurement residuals;	
	
w1partido.mov ~~ w2partido.mov
w1sindicato   ~~ w1sindicato
w1profesional ~~ w2profesional
w1caridad     ~~ w2caridad

w1JJVV        ~~ w2JJVV
w1religiosa   ~~ w2religiosa 
w1deportiva   ~~ w2deportiva

w1c03         ~~ w2c03
w1c02         ~~ w2c02
w1c04         ~~ w2c04

# constrains

lambda1 == 5 - lambda2 - lambda3 - lambda4 - lambda5
lambda6 == 3 - lambda6 - lambda7 - lambda8
lambda9 == 3 - lambda9 - lambda10 - lambda11
"

fit_conf <- sem(modelo.conf, data=w1w2_wide, parameterization= "delta", estimator="wlsmv", 
                    ordered=c("w1JJVV","w1religiosa","w1partido.mov","w1sindicato","w1profesional",
                              "w1caridad","w1AAEE","w1deportiva","w1otra","w1c02","w1c03","w1c04",
                              "w2JJVV","w2religiosa","w2partido.mov","w2sindicato","w2profesional",
                              "w2caridad","w2AAEE","w2deportiva","w2otra","w2c02","w2c03","w2c04"))

summary(fit_conf, fit.measures=TRUE, standardized=TRUE, rsquare=TRUE)

